import React from 'react';
import RootStack from './screens';
import {AuthProvider} from './Context/AuthContext';

const App = () => {
  return (
    <AuthProvider>
      <RootStack />
    </AuthProvider>
  );
};

export default App;
