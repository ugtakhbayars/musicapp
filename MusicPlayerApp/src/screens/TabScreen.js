import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import SettingScreen from './Tabs/SettingScreen';
import HomeScreen from './Tabs/HomeScreen';
import FavorScreen from './Tabs/FavorScreen';

const Tabs = createBottomTabNavigator();

const TabScreen = () => {
  return (
    <Tabs.Navigator initialRouteName="Home">
      <Tabs.Screen name="Artist" component={HomeScreen} />
      <Tabs.Screen name="Favourite" component={FavorScreen} />
      <Tabs.Screen name="Podcast" component={FavorScreen} />
      <Tabs.Screen name="Setting" component={SettingScreen} />
    </Tabs.Navigator>
  );
};

export default TabScreen;
