import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';

import MiniPlayer from '../../components/MiniPlayer/MiniPlayer';

import {datas} from '../../Context/data_temp';

const PlayerScreen = ({route}) => {
  const playSong = route.params.playSong;
  return (
    <View style={styles.container}>
      <View style={styles.topSection}></View>
      <View style={styles.downSection}>
        <View style={styles.playLogo}>
          <View style={styles.playLogoInner}>
            <Image style={styles.logo} source={playSong.artwork} />
          </View>
        </View>
        {/* player */}
        <MiniPlayer song={playSong} />
      </View>
    </View>
  );
};

export default PlayerScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
  },
  logo: {
    width: '100%',
    height: '100%',
    borderRadius: 100,
    backgroundColor: 'white',
  },
  playLogo: {
    display: 'flex',
    top: -100,
    width: 200,
    height: 200,
    backgroundColor: '#3441A0',
    borderRadius: 100,
    zIndex: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  playLogoInner: {
    width: 150,
    height: 150,
    borderRadius: 75,
  },
  playImage: {
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  topSection: {
    flex: 1,
    backgroundColor: '#3441A0',
  },

  downSection: {
    flex: 3,
    alignItems: 'center',
  },
});
