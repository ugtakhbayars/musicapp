import React, {useState, useContext} from 'react';
import {useNavigation} from '@react-navigation/native';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';

import InputText from '../../components/CustomInput';

import {AuthContext} from '../../Context/AuthContext';

const SigninScreen = ({navigation}) => {
  // const navigation = useNavigation();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const ctx = useContext(AuthContext);

  const passwordHandler = value => {
    // console.log('Password: ', value);
    setPassword(value);
  };

  const emailHandler = value => {
    // console.log('Email: ', value);
    setEmail(value);
  };

  const login = () => {
    // console.log('Logging...');
    ctx.loginUser(email, password);
    navigation.navigate('Home');
  };
  return (
    <View style={styles.container}>
      {console.log('Signin Screen: ', ctx.user)}
      <View style={styles.header}>
        <Text style={styles.header__text}>Welcome to Music App</Text>
      </View>
      <Animatable.View animation="fadeInUpBig" style={styles.footer}>
        <InputText label="Email" onChangeText={emailHandler} />
        <InputText label="Password" logoShow onChangeText={passwordHandler} />
        <TouchableOpacity>
          <Text style={styles.forgetPassword}>Forget Password?</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={login}>
          <LinearGradient
            style={styles.startButton}
            colors={['#3441A0', '#5D66B2']}>
            <Text style={styles.buttonText}>Sign In</Text>
          </LinearGradient>
        </TouchableOpacity>
        <View style={styles.down}>
          <Text>Don’t have an account?</Text>
          <TouchableOpacity
            style={{marginLeft: 10}}
            onPress={() => navigation.navigate('Signup')}>
            <Text style={{color: '#3441A0', fontWeight: 'bold'}}>SignUp</Text>
          </TouchableOpacity>
        </View>
      </Animatable.View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#3441A0',
  },
  header: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingHorizontal: 30,
  },
  header__text: {
    color: '#FFFFFF',
    fontSize: 30,
    fontWeight: 'bold',
  },
  footer: {
    flex: 3,
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 30,
    paddingVertical: 40,
  },
  forgetPassword: {
    color: '#555555',
    textAlign: 'right',
    paddingVertical: 10,
    fontWeight: 'bold',
    color: '#3441A0',
  },
  startButton: {
    width: '100%',
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    flexDirection: 'row',
    marginVertical: 20,
  },
  buttonText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontWeight: 'bold',
  },
  down: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
  },
});

export default SigninScreen;
