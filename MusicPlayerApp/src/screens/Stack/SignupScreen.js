import React, {useState, useContext} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import {AuthContext} from '../../Context/AuthContext';

import InputText from '../../components/CustomInput';
import TextButton from '../../components/CustomTextButton';

const SignupScreen = ({navigation}) => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const ctx = useContext(AuthContext);

  const nameHandler = value => {
    // console.log('name: ', value);
    setName(value);
  };
  const passwordHandler = value => {
    // console.log('Password: ', value);
    setPassword(value);
  };

  const emailHandler = value => {
    // console.log('Email: ', value);
    setEmail(value);
  };

  const register = () => {
    // console.log('Logging...');
    ctx.loginUser(email, password);
    navigation.navigate('Home');
  };
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.header__text}>Welcome to Music App</Text>
      </View>
      <Animatable.View animation="fadeInUpBig" style={styles.footer}>
        <InputText label="Username" onChangeText={nameHandler} />
        <InputText label="Email" onChangeText={emailHandler} />
        <InputText label="Password" logoShow onChangeText={passwordHandler} />
        <TouchableOpacity>
          <Text style={styles.forgetPassword}>Agree Term Conditions</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            console.log('Email: ', email);
            console.log('Password: ', password);
          }}>
          <LinearGradient
            style={styles.startButton}
            colors={['#3441A0', '#5D66B2']}>
            <Text style={styles.buttonText}>Sign Up</Text>
          </LinearGradient>
        </TouchableOpacity>
        <View style={styles.down}>
          <Text>Don’t have an account?</Text>
          <TouchableOpacity onPress={register}>
            <Text style={{color: '#3441A0', fontWeight: 'bold'}}>Sign In</Text>
          </TouchableOpacity>
        </View>
      </Animatable.View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#3441A0',
  },
  header: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingHorizontal: 30,
  },
  header__text: {
    color: '#FFFFFF',
    fontSize: 30,
    fontWeight: 'bold',
  },
  footer: {
    flex: 2,
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 30,
    paddingVertical: 40,
  },
  forgetPassword: {
    color: '#555555',
    textAlign: 'center',
    paddingVertical: 10,
    fontWeight: 'bold',
    color: '#3441A0',
  },
  startButton: {
    width: '100%',
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    flexDirection: 'row',
    marginVertical: 15,
  },
  buttonText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontWeight: 'bold',
  },
  down: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
  },
});

export default SignupScreen;
