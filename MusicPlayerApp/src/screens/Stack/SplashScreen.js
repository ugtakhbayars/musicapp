import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const SplashScreen = ({navigation}) => {
  const fadeIn = {
    from: {
      opacity: 0,
    },
    to: {
      opacity: 1,
    },
  };
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Animatable.Image
          style={styles.logo}
          source={require('../../../assets/images/logo/logo1.png')}
          animation={fadeIn}
          duration={2000}
        />
      </View>
      <Animatable.View animation="fadeInUpBig" style={styles.footer}>
        <Text style={styles.footer__title}>
          Listening with great songs or podcast
        </Text>
        <View style={styles.footer__button}>
          <TouchableOpacity
            onPress={() => {
              console.log('To Signin');
              navigation.navigate('Signin');
            }}>
            <LinearGradient
              style={styles.startButton}
              colors={['#5D66B2', '#5D66B2']}>
              <Text style={styles.buttonText}>Get Started</Text>
              <MaterialIcons name="navigate-next" size={20} color="white" />
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </Animatable.View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  logo: {
    width: 300,
    height: 300,
  },
  header: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'teal',
  },
  footer: {
    flex: 1,
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
    backgroundColor: '#3441A0',
    paddingHorizontal: 30,
    paddingVertical: 40,
  },
  footer__title: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontSize: 30,
  },
  footer__button: {
    alignItems: 'flex-end',
    marginTop: 50,
  },
  startButton: {
    width: 150,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    flexDirection: 'row',
  },
  buttonText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default SplashScreen;
