import React, {useContext} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
// import auth from '@react-native-firebase/auth';

import HomeScreen from './Tabs/HomeScreen';
import SplashScreen from './Stack/SplashScreen';
import SigninScreen from './Stack/SigninScreen';
import SignupScreen from './Stack/SignupScreen';
import PlayerScreen from './Stack/PlayerScreen';
// import TabScreen from './TabScreen';

import {AuthContext} from '../Context/AuthContext';
import {useEffect} from 'react';

const Stack = createStackNavigator();

const StackScreen = ({navigation}) => {
  const authCtx = useContext(AuthContext);

  const onAuthStateChange = user => {
    // console.log('State changed...');
    // console.log('User data: ', user);
    authCtx.setUser(user);
  };

  useEffect(() => {
    // const subscriber = auth().onAuthStateChanged(onAuthStateChange);
    // return subscriber;
  }, []);

  return (
    <Stack.Navigator initialRouteName={'Splash'}>
      <Stack.Screen
        name="Splash"
        component={SplashScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Signin"
        component={SigninScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Signup"
        component={SignupScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen name="Song" component={PlayerScreen} />
      <Stack.Screen name="Home" component={HomeScreen} />
      {/* <Stack.Screen
        name="Tab"
        component={TabScreen}
        options={{headerShown: false}}
      /> */}
    </Stack.Navigator>
  );
};

export default StackScreen;
