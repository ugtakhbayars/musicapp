import React, {useState} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';

import SongCard from '../../components/SongCard.js';
import ArtistCard from '../../components/ArtistCard.js';

import Header from '../../components/Header';
import TabBar from '../../components/TabBar';
import List from '../../components/List';

import {datas} from '../../Context/data_temp';

const HomeScreen = () => {
  const ListData = [
    {title: 'Artist'},
    {title: 'Songs'},
    {title: 'Podcast'},
    {title: 'Radio'},
    {title: 'FM'},
    {title: 'AM'},
  ];

  const [selectedTab, setSelectedTab] = useState('Artist');
  const songs = datas.filter(data => data.type === 'song');
  const podcasts = datas.filter(data => data.type === 'podcast');
  return (
    <View style={styles.container}>
      <Header />
      <TabBar
        lists={ListData}
        selectedTab={selectedTab}
        setSelect={setSelectedTab}
      />
      {selectedTab === 'Songs' && <List Component={SongCard} datas={songs} />}
      {selectedTab === 'Podcast' && (
        <List Component={SongCard} datas={podcasts} />
      )}
      {selectedTab === 'Artist' && (
        <List Component={ArtistCard} datas={datas} />
      )}
    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  header: {
    width: '90%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  filter: {
    width: '80%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  avatar: {
    width: 50,
    height: 50,
    backgroundColor: 'pink',
    borderRadius: 50,
    paddingVertical: 5,
  },
  image: {
    width: '100%',
    height: '100%',
    borderRadius: 50,
  },
});
