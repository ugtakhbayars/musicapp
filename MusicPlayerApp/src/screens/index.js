import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import StackScreen from './StackScreen';

const RootStack = () => {
  return (
    <NavigationContainer>
      <StackScreen />
    </NavigationContainer>
  );
};

export default RootStack;
