import React, {useState} from 'react';
// import auth from '@react-native-firebase/auth';
import {createContext} from 'react';

export const AuthContext = createContext();

export const AuthProvider = ({children}) => {
  const [isLogging, setIsLogging] = useState(false);
  const [user, setUser] = useState(null);

  const loginUser = async (email, password) => {
    try {
      console.log('User Login: ', email);
      // await auth().signInWithEmailAndPassword(email, password);
    } catch (err) {
      console.log('Error : Login ==>', err);
    }
  };

  const registerUser = async (email, password) => {
    try {
      console.log('User Register: ', email);
      // await auth().createUserWithEmailAndPassword(email, password);
    } catch (err) {
      console.log('Error : Register ==>', err);
    }
  };

  const logOut = async () => {
    try {
      console.log('User Logout: ', email);
      // await auth().signOut();
      setUser(null);
    } catch (err) {
      console.log('Error : Logout ==>', err);
    }
  };

  return (
    <AuthContext.Provider
      value={{
        user,
        setUser,
        loginUser,
        registerUser,
        logOut,
        isLogging,
        setIsLogging,
      }}>
      {children}
    </AuthContext.Provider>
  );
};
