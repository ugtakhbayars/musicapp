import React, {useState} from 'react';
import {StyleSheet, Text, View, TextInput} from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';

const CustomInput = ({label, logoShow, onChangeText}) => {
  const [show, setShow] = useState(true);

  return (
    <View style={styles.container}>
      <Text style={styles.inputText}>{label}</Text>
      <View style={styles.inputView}>
        <View style={styles.inputView__left}>
          <FontAwesome name="user-o" color="#3441A0" size={20} />
          <TextInput
            style={styles.input}
            placeholder={label}
            secureTextEntry={logoShow && show}
            onChangeText={text => onChangeText(text)}
          />
        </View>
        {logoShow ? (
          <Entypo
            name={show ? 'eye-with-line' : 'eye'}
            color="#3441A0"
            size={20}
            onPress={() => setShow(!show)}
          />
        ) : (
          <Feather name="check-circle" color="#3441A0" size={20} />
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 25,
    borderBottomWidth: 1,
    borderBottomColor: '#e2e2e2',
  },
  inputView: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  inputView__left: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  input: {
    fontSize: 18,
    color: '#3441B0',
    marginLeft: 10,
  },
  inputText: {
    color: '#7c7c7c',
    fontSize: 18,
    fontWeight: 'bold',
    paddingVertical: 3,
    color: '#3441A0',
  },
});

export default CustomInput;
