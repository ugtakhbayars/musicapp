import React, {useState} from 'react';
import {StyleSheet, Text, ScrollView} from 'react-native';
import SongCard from './SongCard';
import ArtistCard from './ArtistCard';
import * as Animatable from 'react-native-animatable';

const List = ({Component, datas}) => {
  return (
    <Animatable.View
      style={styles.container}
      animation="fadeInUpBig"
      duration={1500}>
      <ScrollView>
        {datas.map((data, key) => (
          <Component key={key} data={data} />
        ))}
      </ScrollView>
    </Animatable.View>
  );
};

export default List;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: '#3441A0',
    paddingVertical: 15,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
});
