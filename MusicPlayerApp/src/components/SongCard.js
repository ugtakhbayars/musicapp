import React, {useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
const avatar = require('../../assets/images/logo/logo3.png');

const SongCard = ({data}) => {
  const navigation = useNavigation();
  const [show, setShow] = useState(false);
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => {
        console.log('------Songcard-------', data);
        navigation.navigate('Song', {playSong: data});
      }}>
      <View style={styles.avatar}>
        <Image style={styles.avatarImage} source={data.artwork} />
      </View>
      <View style={styles.textContent}>
        <Text style={styles.title}>{data.title}</Text>
        <Text style={styles.subTitle}>{data.artist}</Text>
      </View>
      <TouchableOpacity onPress={() => setShow(!show)}>
        <AntDesign name={show ? 'heart' : 'hearto'} size={28} color="#3441A0" />
      </TouchableOpacity>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 80,
    width: '95%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 5,
    paddingHorizontal: 20,
    marginHorizontal: 10,
    borderRadius: 20,
    alignItems: 'center',
    backgroundColor: '#F6F6F6',
  },
  avatar: {
    backgroundColor: 'red',
    width: 50,
    height: 50,
    borderRadius: 25,
  },
  avatarImage: {
    width: '100%',
    height: '100%',
    borderRadius: 25,
  },
  textContent: {
    width: '65%',
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#57586B',
    marginBottom: 5,
  },
  subTitle: {
    fontSize: 15,
    color: '#C6C7CD',
  },
});

export default SongCard;
