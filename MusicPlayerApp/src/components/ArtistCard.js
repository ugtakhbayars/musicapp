import React, {useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';

const avatar = require('../../assets/images/logo/logo3.png');

const ArtistCard = ({data}) => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => {
        console.log('Clicked');
      }}>
      <View style={styles.avatar}>
        <Image style={styles.avatarImage} source={avatar} />
      </View>
      <View style={styles.textContent}>
        <Text style={styles.title}>{data.title}</Text>
        <Text style={styles.subTitle}>{data.album}</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 150,
    width: '95%',
    justifyContent: 'center',
    marginVertical: 5,
    paddingHorizontal: 10,
    marginHorizontal: 10,
    borderRadius: 20,
    alignItems: 'center',
    backgroundColor: '#F6F6F6',
  },
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 25,
  },
  avatarImage: {
    width: '100%',
    height: '100%',
    borderRadius: 25,
  },
  textContent: {
    width: '90%',
    alignItems: 'center',
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#57586B',
    marginBottom: 5,
  },
  subTitle: {
    fontSize: 15,
    color: '#C6C7CD',
  },
});

export default ArtistCard;
