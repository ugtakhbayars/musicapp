import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const CustomButton = ({text, colors, onPress}) => {
  return (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate('Signin');
      }}>
      <LinearGradient style={styles.startButton} colors={colors}>
        <Text style={styles.buttonText}>{text}</Text>
        {/* <MaterialIcons name="navigate-next" size={20} color="white" /> */}
      </LinearGradient>
    </TouchableOpacity>
  );
  <TouchableOpacity onPress={register}>
    <Text style={{color: '#3441A0', fontWeight: 'bold'}}>Sign In</Text>
  </TouchableOpacity>;
};

export default CustomButton;

const styles = StyleSheet.create({
  startButton: {
    width: 150,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    flexDirection: 'row',
  },
  buttonText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontWeight: 'bold',
  },
});
