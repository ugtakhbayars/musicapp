import React from 'react';
import {View, StyleSheet} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

const Header = () => {
  return (
    <View style={styles.container}>
      <FontAwesome name="search" size={28} color="#3441A0" />
      <FontAwesome name="music" size={35} color="#3441A0" />
      <FontAwesome name="th-large" size={28} color="#3441A0" />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 70,
    paddingHorizontal: 20,
    alignItems: 'center',
    backgroundColor: '#E9E9E9',
  },
});

export default Header;
