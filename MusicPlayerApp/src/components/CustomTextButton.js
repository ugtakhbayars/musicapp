import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const CustomTextButton = ({text, textColor, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <Text style={{color: textColor, fontWeight: 'bold'}}>{text}</Text>
    </TouchableOpacity>
  );
};

export default CustomTextButton;
