import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import Slider from 'react-native-slider';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import TrackPlayer from 'react-native-track-player';

TrackPlayer.updateOptions({
  stopWithApp: false,
  capabilities: [TrackPlayer.CAPABILITY_PLAY, TrackPlayer.CAPABILITY_PAUSE],
  compactCapabilities: [
    TrackPlayer.CAPABILITY_PLAY,
    TrackPlayer.CAPABILITY_PAUSE,
  ],
});

// const tracks = [
//   {id: 1, url: require('../assets/sounds/track1.mp3'), title: 'Track 1'},
//   {id: 2, url: require('../assets/sounds/track2.mp3'), title: 'Track 2'},
// ];

const MiniPlayer = ({song}) => {
  const [songSecond, setSongSecond] = useState(0.8);
  const [isPlaying, setIsPlaying] = useState(true);

  const durationPercent = second => {};

  const play = async () => {
    setIsPlaying(!isPlaying);
    if (isPlaying) {
      TrackPlayer.play();
      // console.log('Play');
    } else {
      TrackPlayer.pause();
      // console.log('Pause');
    }
  };
  // const stop = async () => {
  // };
  const prev = async () => {
    console.log('Prev');
    TrackPlayer.skipToPrevious();
  };
  const next = async () => {
    console.log('Next');
    TrackPlayer.skipToNext();
  };
  const setupPlayer = async () => {
    try {
      await TrackPlayer.setupPlayer();
      await TrackPlayer.add(song);
      console.log('TrackPlayer Initialized.');
    } catch (err) {
      console.log('Error: ', err);
    }
  };

  useEffect(() => {
    setupPlayer();
    return () => TrackPlayer.destroy();
  }, []);

  return (
    <View style={styles.playControls}>
      <View style={styles.playName}>
        <Text style={styles.title}>{song.title}</Text>
        <Text style={styles.subTitle}>{song.artist}</Text>
      </View>
      <View style={styles.sliderContainer}>
        <Text>1:20</Text>
        <Slider
          style={styles.slider}
          minimumTrackTintColor="#3441A0"
          thumbTintColor="#3441A0"
          value={songSecond}
        />
        <Text>3:50</Text>
      </View>
      <View style={styles.buttonsContainer}>
        <FontAwesome name="step-backward" size={28} color="#3441A0" />
        <TouchableOpacity onPress={play}>
          <FontAwesome
            name={isPlaying ? 'play' : 'pause'}
            size={35}
            color="#3441A0"
          />
        </TouchableOpacity>
        <FontAwesome name="step-forward" size={28} color="#3441A0" />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  playControls: {
    // backgroundColor: 'green',
    flex: 1,
    width: '90%',
    top: -80,
    alignItems: 'center',
  },
  playName: {
    alignItems: 'center',
  },
  title: {
    fontSize: 25,
    fontWeight: 'bold',
    paddingVertical: 0,
  },
  subTitle: {
    fontSize: 16,
    fontWeight: '600',
    paddingVertical: 0,
  },
  sliderContainer: {
    width: '100%',
    height: '15%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 15,
  },
  buttonsContainer: {
    width: '100%',
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    backgroundColor: '#EEEEEE',
  },
  slider: {
    width: '80%',
  },
});

export default MiniPlayer;
/**
 * 



const App = () => {
  //     url: 'https://audio-previews.elements.envatousercontent.com/files/103682271/preview.mp3',
  const [songSecond, setSongSecond] = useState(50);
  const [playBtn, setPlayBtn] = useState(true);

  

  return <SafeAreaView></SafeAreaView>;
};

export default App;

 */
