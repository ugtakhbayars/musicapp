import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import * as Animatable from 'react-native-animatable';

const TabBar = ({lists, selectedTab, setSelect}) => {
  return (
    <Animatable.View
      animation="fadeInUpBig"
      duration={1500}
      style={styles.container}>
      <ScrollView horizontal>
        {lists.map((list, key) => (
          <TouchableOpacity
            style={styles.scroll}
            key={key}
            onPress={() => {
              setSelect(list.title);
            }}>
            <View style={styles.text}>
              <Text
                style={[
                  styles.title,
                  selectedTab === list.title ? styles.selected : '',
                ]}>
                {list.title}
              </Text>
            </View>
          </TouchableOpacity>
        ))}
      </ScrollView>
    </Animatable.View>
  );
};

export default TabBar;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    paddingLeft: 25,
  },
  text: {
    paddingRight: 25,
    paddingVertical: 20,
  },
  title: {
    fontSize: 20,
    color: '#333333',
  },
  selected: {
    fontWeight: 'bold',
    borderBottomWidth: 3,
  },
});
