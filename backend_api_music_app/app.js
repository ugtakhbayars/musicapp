const express = require("express");
const dotenv = require("dotenv");

dotenv.config({ path: "./config/config.env" });
const app = express();
app.use(express.json());

app.get("/songs", (req, res) => {
  res.status(200).json({
    status: "OK",
    data: "All songs will send to users.",
  });
});

app.get("/podcasts", (req, res) => {
  res.status(200).json({
    status: "OK",
    data: "All podcasts will send to users.",
  });
});

app.listen(process.env.PORT, () =>
  console.log(`Server is running at ${process.env.PORT} port.`)
);
